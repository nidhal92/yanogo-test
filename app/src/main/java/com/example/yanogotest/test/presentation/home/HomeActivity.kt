package com.example.yanogotest.test.presentation.home

import android.content.Intent
import android.view.View
import androidx.lifecycle.ViewModelProviders
import com.example.yanogotest.R
import com.example.yanogotest.databinding.ActivityHomeBinding
import com.example.yanogotest.test.presentation.base.BaseActivity
import com.example.yanogotest.test.presentation.sellsList.SellsActivity

class HomeActivity : BaseActivity<ActivityHomeBinding>(),HomeNavigator {


    private lateinit var viewModel: HomeViewModel

    override fun navigateToSellsList() {
        startActivity(Intent(this@HomeActivity, SellsActivity::class.java))
    }

    override fun setViewModel() {
        viewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)

    }

    override fun init() {
        viewModel.setNavigator(this)
        getDataBinding()?.viewModel=viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_home
    }
}
