package com.example.yanogotest.test.data.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Sellings(
    @SerializedName("topic") var topic: String,
    @SerializedName("order_id") var order_id: String,
    @SerializedName("receiver_id") var receiver_id: String,
    @SerializedName("name") var name: String,
    @SerializedName("sender_name") var sender_name: String,
    @SerializedName("delivery_date") var delivery_date: String,
    @SerializedName("number") var number: Int,
    @SerializedName("status") var status: String,
    @SerializedName("is_tour_planning_responsible") var is_tour_planning_responsible: Int,
    @SerializedName("receivers_size") var receivers_size: Int,
    @SerializedName("is_buyer") var is_buyer: Int,
    @SerializedName("receiver_is_registered") var receiver_is_registered: Int,
    @SerializedName("business_partner") var business_partner: String
) : Serializable