package com.example.yanogotest.test.data.entities

import com.google.gson.annotations.SerializedName

data class Stats (
    @SerializedName("count") var count: String,
    @SerializedName("status") var status: String
    )
