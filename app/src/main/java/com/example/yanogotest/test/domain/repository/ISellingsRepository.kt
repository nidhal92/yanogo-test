package com.example.yanogotest.test.domain.repository

import com.example.yanogotest.test.data.entities.SellingsListResponse
import io.reactivex.Observable

interface ISellingsRepository {
    fun getOpenSellings(filter: String, page: String): Observable<SellingsListResponse>
    fun getCompletedSellings(filter: String, date: String, page: String): Observable<SellingsListResponse>
}