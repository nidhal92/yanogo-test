package com.example.yanogotest.test.presentation.sellsList

import androidx.lifecycle.ViewModelProviders
import com.example.yanogotest.R
import com.example.yanogotest.databinding.ActivitySellsBinding
import com.example.yanogotest.test.presentation.base.BaseActivity
import com.example.yanogotest.test.presentation.sellsList.adapter.TabFragmentAdapter
import com.example.yanogotest.test.presentation.sellsList.fragment.CompletedFragment
import com.example.yanogotest.test.presentation.sellsList.fragment.OpenFragment


class SellsActivity : BaseActivity<ActivitySellsBinding>(), SellingsNavigator {

    private lateinit var viewModel: SellingsViewModel
    override fun setViewModel() {
        viewModel = ViewModelProviders.of(this).get(SellingsViewModel::class.java)
    }

    override fun init() {
        viewModel.setNavigator(this)
        getDataBinding()?.viewModel = viewModel
        var adapter = TabFragmentAdapter(supportFragmentManager)
        adapter.addFragment(
            OpenFragment(),
            "Open" + "(" + viewModel.getOpenSellingsCount().get() + ")"
        )
        adapter.addFragment(
            CompletedFragment(),
            "Completed" + "(" + viewModel.getCompletedSellingsCount().get() + ")"
        )

        getDataBinding()?.sellsViewPager?.adapter = adapter
        getDataBinding()?.sellsTablayout?.setupWithViewPager(getDataBinding()?.sellsViewPager)
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_sells
    }
}
