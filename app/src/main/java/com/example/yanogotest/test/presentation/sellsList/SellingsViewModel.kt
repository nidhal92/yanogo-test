package com.example.yanogotest.test.presentation.sellsList

import android.annotation.SuppressLint
import android.util.Log
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableField
import com.example.yanogotest.test.data.entities.Sellings
import com.example.yanogotest.test.data.entities.SellingsListResponse
import com.example.yanogotest.test.domain.useCases.GetCompletedSellingsListUseCase
import com.example.yanogotest.test.domain.useCases.GetOpenSellingsListUseCase
import com.example.yanogotest.test.presentation.base.BaseViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@SuppressLint("CheckResult")

class SellingsViewModel : BaseViewModel<SellingsNavigator>() {
    private val completedCount = ObservableField<String>("3")
    private lateinit var completedSellingsResponse: SellingsListResponse
    private lateinit var openSellingsResponse: SellingsListResponse
    private var completedSellingsListUseCase = GetCompletedSellingsListUseCase()
    private var openSellingsListUseCase = GetOpenSellingsListUseCase()
    private var openSellingsArray = ObservableArrayList<Sellings>()
    private var completedSellingsArray = ObservableArrayList<Sellings>()
    private var openCount = ObservableField<String>("34")
    val progressVisible = ObservableBoolean()

    init {
        completedSellingsListUseCase.execute("completed", "25.03.20", "1")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                handleCompletedData(it)
            }
        openSellingsListUseCase.execute("open", "1")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

            .subscribe {
                handleOpenData(it)
            }
    }

    private fun handleOpenData(result: GetOpenSellingsListUseCase.Result?) {
        progressVisible.set(result == GetOpenSellingsListUseCase.Result.Loading)
        when (result) {

            is GetOpenSellingsListUseCase.Result.Success -> {

                openSellingsResponse = result.sellingsResponse
                openSellingsArray.addAll(openSellingsResponse.data.items)
                openSellingsResponse.data.stats.map {
                    if (it.status == "open") {
                        Log.e("open",it.count)
                        openCount.set(it.count)
                        openCount.notifyChange()
                    }
                }



            }
            is GetOpenSellingsListUseCase.Result.Failure -> {
                Log.d("ERROR", result.throwable.message)
            }
        }
    }

    private fun handleCompletedData(result: GetCompletedSellingsListUseCase.Result?) {
        progressVisible.set(result == GetCompletedSellingsListUseCase.Result.Loading)
        when (result) {

            is GetCompletedSellingsListUseCase.Result.Success -> {

                completedSellingsResponse = result.sellingsResponse
                completedSellingsArray.addAll(completedSellingsResponse.data.items)
                completedSellingsResponse.data.stats.map {
                    if (it.status == "completed") {
                        Log.e("completed",it.count)
                        completedCount.set(it.count)
                        completedCount.notifyChange()
                    }
                }

            }
            is GetCompletedSellingsListUseCase.Result.Failure -> {
                Log.d("ERROR", result.throwable.message)
            }
        }
    }

    fun getCompletedSellingsArray(): ObservableArrayList<Sellings> {
        return completedSellingsArray
    }

    fun getOpenSellingsArray(): ObservableArrayList<Sellings> {
        return openSellingsArray
    }

    fun getCompletedSellingsCount(): ObservableField<String> {
        return completedCount
    }

    fun getOpenSellingsCount(): ObservableField<String> {
        return openCount
    }
}