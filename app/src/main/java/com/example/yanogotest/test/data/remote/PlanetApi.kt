package com.example.yanogotest.test.data.remote


import com.example.yanogotest.test.data.entities.SellingsListResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query

interface PlanetApi {
    @GET(ApiEndPoints.GET_SELLS)
    fun getOpenSellings(@Header("Authorization") authHeader:String,@Query("filter") filter: String, @Query("page") page: String): Observable<SellingsListResponse>

    @GET(ApiEndPoints.GET_SELLS)
    fun getCompletedSellings(@Header("Authorization")  authHeader:String, @Query("filter") filter: String, @Query("date") date: String, @Query("page") page: String): Observable<SellingsListResponse>
}