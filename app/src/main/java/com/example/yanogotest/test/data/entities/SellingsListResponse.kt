package com.example.yanogotest.test.data.entities

import com.google.gson.annotations.SerializedName
import java.io.Serializable

 class SellingsListResponse (
    @SerializedName("status") var status: String,
    @SerializedName("code") var code: Int?,
    @SerializedName("name") var name: String,
    @SerializedName("data") var data: DataResponse

    ):Serializable