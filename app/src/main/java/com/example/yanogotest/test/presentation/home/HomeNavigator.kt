package com.example.yanogotest.test.presentation.home

interface HomeNavigator {

    fun navigateToSellsList()
}
