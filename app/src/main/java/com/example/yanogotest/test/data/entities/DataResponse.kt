package com.example.yanogotest.test.data.entities

import com.google.gson.annotations.SerializedName

data class DataResponse (
    @SerializedName("items") var items: ArrayList<Sellings>,
    @SerializedName("dates") var dates: ArrayList<String>,
    @SerializedName("stats") var stats: ArrayList<Stats>
)
