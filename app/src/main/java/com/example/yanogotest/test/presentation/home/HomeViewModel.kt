package com.example.yanogotest.test.presentation.home

import com.example.yanogotest.test.presentation.base.BaseViewModel

class HomeViewModel : BaseViewModel<HomeNavigator>() {
    fun navigateToSellsList() {
        getNavigator()?.navigateToSellsList()
    }
}