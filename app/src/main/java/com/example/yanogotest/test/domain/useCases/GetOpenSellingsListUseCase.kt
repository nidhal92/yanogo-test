package com.example.yanogotest.test.domain.useCases


import com.example.yanogotest.test.data.entities.SellingsListResponse
import com.example.yanogotest.test.data.repository.SellingsRepositoryImp
import com.example.yanogotest.test.domain.repository.ISellingsRepository
import io.reactivex.Observable


class GetOpenSellingsListUseCase {
    private val repository: ISellingsRepository = SellingsRepositoryImp()
    sealed class Result {
        object Loading : Result()
        data class Success(val sellingsResponse: SellingsListResponse) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute(filter: String, page: String): Observable<Result> {
        return repository.getOpenSellings(filter, page)
            .doOnNext { Result.Success(it) }
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)

    }
}