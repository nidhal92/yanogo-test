package com.example.yanogotest.test.presentation.databinding


import android.view.View
import android.widget.ProgressBar
import androidx.databinding.BindingAdapter
import androidx.databinding.BindingConversion
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableBoolean
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.yanogotest.test.data.entities.Sellings
import com.example.yanogotest.test.presentation.sellsList.adapter.SellingsListAdapter


@BindingAdapter("visibility")
 fun setVisibility(progressBar: ProgressBar, state: ObservableBoolean) {
    if (state.get()) progressBar.visibility = View.VISIBLE else progressBar.visibility = View.GONE
}

@BindingAdapter("adapter")
fun bindList(recyclerView: RecyclerView, list: ObservableArrayList<Sellings>) {

    val adapter = SellingsListAdapter(list, recyclerView.context)

    recyclerView.addItemDecoration(DividerItemDecoration(recyclerView.context, 0))
    val manager = LinearLayoutManager(recyclerView.context)
    manager.orientation = LinearLayoutManager.VERTICAL
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.adapter = adapter
}




