package com.example.yanogotest.test.presentation.sellsList.fragment

import androidx.lifecycle.ViewModelProviders
import com.example.yanogotest.R
import com.example.yanogotest.databinding.FragmentOpenBinding
import com.example.yanogotest.test.presentation.base.BaseFragment
import com.example.yanogotest.test.presentation.sellsList.SellingsViewModel


class OpenFragment : BaseFragment<FragmentOpenBinding>() {

    private lateinit var viewModel: SellingsViewModel
    override fun setViewModel() {
        viewModel = ViewModelProviders.of(activity!!).get(SellingsViewModel::class.java)
    }

    override fun init() {
        getViewDataBinding().viewModel=viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_open
    }

}
