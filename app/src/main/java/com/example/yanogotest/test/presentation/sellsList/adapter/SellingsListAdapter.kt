package com.example.yanogotest.test.presentation.sellsList.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.ObservableArrayList
import com.example.yanogotest.R
import com.example.yanogotest.databinding.ItemSellingsLayoutBinding
import com.example.yanogotest.test.data.entities.Sellings
import com.example.yanogotest.test.presentation.base.BaseViewHolder
import com.example.yanogotest.test.presentation.base.ObservableRecyclerViewAdapter


class SellingsListAdapter(var sellingsList: ObservableArrayList<Sellings>, var context: Context) :
    ObservableRecyclerViewAdapter<Sellings, BaseViewHolder<Sellings>>(sellingsList) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Sellings> {
        return PlanetHolder(
            ItemSellingsLayoutBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )

        )
    }

    override fun onBindViewHolder(holder: BaseViewHolder<Sellings>, position: Int) {
        holder.bind(getItem(position))
    }

    inner class PlanetHolder(
        private val binding: ItemSellingsLayoutBinding

    ) : BaseViewHolder<Sellings>(binding.root) {
        override fun bind(item: Sellings) {

            binding.selling = item
            when (item.status) {
                "TOUR_PLANNING" -> {
                    binding.itemImage.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.tour_planning
                        )
                    )
                    binding.itemText.text = "Tour Planing"
                }
                "LOADED" -> {
                    binding.itemImage.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.loaded
                        )
                    )
                    binding.itemText.text = "Loaded"
                }
                "TO_BE_LOADED" -> {
                    binding.itemImage.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.to_be_loaded
                        )
                    )
                    binding.itemText.text = "To be loaded"
                }
                "SOLD" -> {
                binding.itemImage.setImageDrawable(
                    ContextCompat.getDrawable(
                        context,
                        R.drawable.to_be_loaded
                    )
                )
                binding.itemText.text = "Sold"
            }
                "ON_THE_WAY" -> {
                    binding.itemImage.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.ontheway
                        )
                    )
                    binding.itemText.text = "On the way"
                }
                "DELIVERED" -> {
                    binding.itemImage.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.delivered
                        )
                    )
                    binding.itemText.text = "Delivered"
                }"OFFERED" -> {
                    binding.itemImage.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.delivered
                        )
                    )
                binding.itemText.text = "Offered"

            }
                "UNLOADED" -> {
                    binding.itemImage.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.unloaded
                        )
                    )
                    binding.itemText.text = "Unloaded"
                }
                "SLAUGHTERED" -> {
                    binding.itemImage.setImageDrawable(
                        ContextCompat.getDrawable(
                            context,
                            R.drawable.slaughered
                        )
                    )
                    binding.itemText.text = "Slaughered."
                }
            }

        }


    }
}