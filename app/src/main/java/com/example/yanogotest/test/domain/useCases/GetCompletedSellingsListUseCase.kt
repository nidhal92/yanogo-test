package com.example.yanogotest.test.domain.useCases


import com.example.yanogotest.test.data.entities.SellingsListResponse
import com.example.yanogotest.test.data.repository.SellingsRepositoryImp
import com.example.yanogotest.test.domain.repository.ISellingsRepository
import io.reactivex.Observable


class GetCompletedSellingsListUseCase {
    private val repository: ISellingsRepository = SellingsRepositoryImp()
    sealed class Result {
        object Loading : Result()
        data class Success(val sellingsResponse: SellingsListResponse) : Result()
        data class Failure(val throwable: Throwable) : Result()
    }

    fun execute(filter: String, date: String, page: String): Observable<Result> {
        return repository.getCompletedSellings(filter, date, page)
            .doOnNext { Result.Success(it) as Result }
            .map { Result.Success(it) as Result }
            .onErrorReturn { Result.Failure(it) }
            .startWith(Result.Loading)

    }
}