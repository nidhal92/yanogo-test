package com.example.yanogotest.test.data.repository


import com.example.yanogotest.test.data.entities.SellingsListResponse
import com.example.yanogotest.test.data.remote.ApiEndPoints
import com.example.yanogotest.test.data.remote.RetrofitClient
import com.example.yanogotest.test.domain.repository.ISellingsRepository
import io.reactivex.Observable

class SellingsRepositoryImp  : ISellingsRepository {

    override fun getOpenSellings(filter:String,page:String) : Observable<SellingsListResponse> {
        return RetrofitClient.build().getOpenSellings(ApiEndPoints.AUTHORIZATION,filter,page).map { it }

    }

    override fun getCompletedSellings(filter:String,date:String,page:String): Observable<SellingsListResponse> {
        return RetrofitClient.build().getCompletedSellings(ApiEndPoints.AUTHORIZATION,filter, date, page).map { it }

    }
}