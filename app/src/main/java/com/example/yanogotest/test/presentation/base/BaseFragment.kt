package com.example.yanogotest.test.presentation.base


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment


abstract class BaseFragment<T : ViewDataBinding> : Fragment() {
    private var mRootView: View? = null
    private lateinit var mViewDataBinding: T
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setViewModel()

    }

    abstract fun setViewModel()
    abstract fun init()
     fun getViewDataBinding() :T{
         return mViewDataBinding
     }


    private fun performDataBinding() {}

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewDataBinding = DataBindingUtil.inflate<T>(inflater, getLayoutId(), container, false)
        mRootView = mViewDataBinding.root
        init()
        return mRootView
    }

    @LayoutRes
    abstract fun getLayoutId(): Int
}